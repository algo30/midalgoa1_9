
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author HP
 */
public class MidAlgoA1_9 {
    public static void main(String[] args) {
          	
        long start = System.nanoTime();
        
        try {
           //change file textname by yourself
           //Ex. 1.in.txt change to 2.in.txt	
            Path file = Paths.get("D:\\MidAlgo\\MidAlgorithm\\src\\main\\java\\10.in.txt");
            BufferedReader reader = Files.newBufferedReader(file, StandardCharsets.UTF_8);
            String line;
            while(( line = reader.readLine()) != null){
            String text= line ;
            if(Integer.parseInt(text)>=0 && Integer.parseInt(text)<=9){
            System.out.println("Input from file : ");
            
            System.out.print("Check text + text : "+ (text + text));
            System.out.println();
            System.out.println("Converse String to Integer");
            int num1 = Integer.parseInt(text);
            int num2 = Integer.parseInt(text);
            System.out.print("Check num1 + num2 : "+ (num1 + num2));
            System.out.println();
            }else{
                 System.out.println("Out of range");
            }
            }
            reader.close();
        } catch (IOException ex) {
            Logger.getLogger(MidAlgoA1_9.class.getName()).log(Level.SEVERE, null, ex);
        } 

         long end = System.nanoTime();
         System.out.println("Running Time Program is : "+(end-start) * 1E-9+ "secs.");
        
    }
}
